/**
 * magicians.js
 * November 2, 2003
 *
 * @desc Repert generator for the Magicians Alliance.
 * @author Tobias Fünke
 */

console.log('Alliance of Magicians - Member Report');

var magicians = [
  {
    firstName: 'Gob',
    lastName: 'Bluth',
    birthday: new Date('May 4, 1970'),
    deathday: null,
    status: 'banned',
    specialty: 'tricks'
  },
  {
    firstName: 'Harry',
    lastName: 'Houdini',
    birthday: new Date('May 4, 1970'),
    deathday: new Date('October 31, 1926'),
    status: 'active',
    specialty: 'escape'
  },
  {
    fistName: 'David',
    lastName: 'Copperfield',
    birthday: new Date('September 16, 1956'),
    deathday: null,
    status: 'retired',
    specialty: 'illusions'
  },
  {
    firstName: 'David',
    lastName: 'Blaine',
    birthday: new Date('April 4, 1973'),
    deathday: null,
    status: 'active',
    specialty: 'endurance'
  },
  {
    firstName: 'Criss',
    lastName: 'Angel',
    birthday: new Date('December 19, 1967'),
    deathday: null,
    status: 'active',
    specialty: 'illusions'
  }
];

console.log('All Members: ', magicians.length);

console.log("====== Active Members ======");

for(var i = 0; i < magicians.length; i++) {
  // TODO: create a function called isActive that makes the code more readable.
  // TODO: create a function called isAlive that makes the code more readable.
  if(magicians[i].status === 'active' && magicians[i].deathday === null) {
    // TODO: DRY this up into a function
    console.log('\t', magicians[i].lastName + ', ' + magicians[i].firstName);
  }
}

console.log("====== Banned Members ======");

for(var i = 0; i < magicians.length; i++) {
  // TODO: create a function called isBanned that makes the code more readable.
  if(magicians[i].status === 'banned') {
    // TODO: DRY this up into a function
    console.log('\t', magicians[i].lastName + ', ' + magicians[i].firstName);
  }
}
